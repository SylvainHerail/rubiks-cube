#ifndef CUBE_H
#define CUBE_H
#include <windows.h>  // for MS Windows
#include <GL/glut.h>

class Cube
{
    public:
        Cube();
        void displayCubes(GLfloat p_sx1,GLfloat p_sx2,GLfloat p_sy1,GLfloat p_sy2,GLfloat p_sz1,GLfloat p_sz2);
        virtual ~Cube();
        void bittee();
    protected:

    private:
};

#endif // CUBE_H

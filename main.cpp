#include <tgmath.h>
#include <windows.h>  // for MS Windows
#include <GL/glut.h>  // GLUT library
#include <cstdio>
#include "Cube.h"
#include <thread>
#include <iostream>
#include <pthread.h>

using namespace std;

/* Global variables */

char title[] = "Rubik's Cube";
float angle = 0.0f;             // Rotation angle for camera
float rayon = 20.0f;
GLfloat angleCube = 0.0f;       // Rotational angle for cube
GLfloat transcube = 0.0f;
GLfloat transcubey = 0.0f;
GLfloat transcubez = 0.0f;
int refreshMills = 15;          // refresh interval in milliseconds
GLfloat camerax = 3.1f;         // Position x of the camera
GLfloat cameray = 3.1f;         // Position y of the camera
GLfloat cameraz = -20.0f;       // Position z of the camera
GLfloat facecubex = 0.0f;
GLfloat facecubey = 0.0f;
GLfloat facecubez = 0.0f;
int i=0;
GLfloat sx1 = 0.0f ,sx2 = 2.0f, sy1 = 0.0f ,sy2 = 2.0f, sz1 = 0.0f ,sz2 = 2.0f;
bool flag=false;

/* Initialize OpenGL Graphics */

void initGL() {
    glEnable(GL_DEPTH_TEST);   // Enable depth testing for z-culling
    glDepthFunc(GL_LEQUAL);    // Set the type of depth-test
    glShadeModel(GL_SMOOTH);   // Enable smooth shading
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
}

/* Render a color-cube consisting of 6 quads with different colors */

void displayCubes(GLfloat p_sx1,GLfloat p_sx2,GLfloat p_sy1,GLfloat p_sy2,GLfloat p_sz1,GLfloat p_sz2, GLfloat p_angle, GLfloat p_trans){
    glPushMatrix();
    if (p_angle!=0.0){
        glTranslatef(transcube, transcubey, 0.0f);  // Move right and into the screen
        //GLdouble translate[16] = {1,0,0,0 ,0,cos(90),-sin(90),0 ,0,sin(90),cos(90),0 ,0,0,0,0};
        //glMultMatrixd(translate);
        glRotatef(p_angle, 0.0f, 0.0f, 1.0f);  // Rotate about (1,1,1)-axis [NEW]
        glTranslatef(0.0, -transcubey/100, 0.0f);
    }

    glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads

    //Create all the face for each cube
    // Top face (y = 1.0f)
    glColor3f(0.0f, 1.0f, 0.0f);     // Green
    glVertex3f( p_sx1, p_sy1, p_sz2);
    glVertex3f(p_sx2, p_sy1, p_sz2);
    glVertex3f(p_sx2, p_sy1,  p_sz1);
    glVertex3f( p_sx1, p_sy1,  p_sz1);

    // Bottom face (y = -1.0f)
    glColor3f(1.0f, 0.5f, 0.0f);     // Orange
    glVertex3f( p_sx1, p_sy2,  p_sz1);
    glVertex3f(p_sx2, p_sy2,  p_sz1);
    glVertex3f(p_sx2, p_sy2, p_sz2);
    glVertex3f( p_sx1, p_sy2, p_sz2);

    // Front face  (z = 1.0f)
    glColor3f(1.0f, 0.0f, 0.0f);     // Red
    glVertex3f( p_sx1,  p_sy1, p_sz1);
    glVertex3f(p_sx2,  p_sy1, p_sz1);
    glVertex3f(p_sx2, p_sy2, p_sz1);
    glVertex3f( p_sx1, p_sy2, p_sz1);

    // Back face (z = -1.0f)
    glColor3f(1.0f, 1.0f, 0.0f);     // Yellow
    glVertex3f( p_sx1, p_sy2, p_sz2);
    glVertex3f(p_sx2, p_sy2, p_sz2);
    glVertex3f(p_sx2,  p_sy1, p_sz2);
    glVertex3f( p_sx1,  p_sy1, p_sz2);

    // Left face (x = -1.0f)
    glColor3f(0.0f, 0.0f, 1.0f);     // Blue
    glVertex3f(p_sx2,  p_sy1,  p_sz1);
    glVertex3f(p_sx2,  p_sy1, p_sz2);
    glVertex3f(p_sx2, p_sy2, p_sz2);
    glVertex3f(p_sx2, p_sy2,  p_sz1);

    // Right face (x = 1.0f)
    glColor3f(1.0f, 0.0f, 1.0f);     // Magenta
    glVertex3f(p_sx1,  p_sy1, p_sz2);
    glVertex3f(p_sx1,  p_sy1,  p_sz1);
    glVertex3f(p_sx1, p_sy2,  p_sz1);
    glVertex3f(p_sx1, p_sy2, p_sz2);
    glEnd();  // End of drawing color-cube
    glPopMatrix();
}

/* Handler for window-repaint event. Called back when the window first appears and
   whenever the window needs to be re-painted. */

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
    glLoadIdentity();
    gluLookAt(0.0f+camerax,cameray,0.0f-cameraz, 3.1f,3.1f,3.1f, 0.0f,1.0f,0.0f);//move the camera

    //Create the first face
    displayCubes(sx1,sx2,sy1,sy2,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+2.1,sx2+2.1,sy1,sy2,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+4.2,sx2+4.2,sy1,sy2,sz1,sz2, angleCube, transcube);

    displayCubes(sx1,sx2,sy1+2.1,sy2+2.1,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+2.1,sx2+2.1,sy1+2.1,sy2+2.1,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+4.2,sx2+4.2,sy1+2.1,sy2+2.1,sz1,sz2, angleCube, transcube);

    displayCubes(sx1,sx2,sy1+4.2,sy2+4.2,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+2.1,sx2+2.1,sy1+4.2,sy2+4.2,sz1,sz2, angleCube, transcube);
    displayCubes(sx1+4.2,sx2+4.2,sy1+4.2,sy2+4.2,sz1,sz2, angleCube, transcube);

    //Create the second line
    displayCubes(sx1,sx2,sy1,sy2,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1,sy2,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1,sy2,sz1+2.1,sz2+2.1,0,0);

    displayCubes(sx1,sx2,sy1+2.1,sy2+2.1,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1+2.1,sy2+2.1,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1+2.1,sy2+2.1,sz1+2.1,sz2+2.1,0,0);

    displayCubes(sx1,sx2,sy1+4.2,sy2+4.2,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1+4.2,sy2+4.2,sz1+2.1,sz2+2.1,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1+4.2,sy2+4.2,sz1+2.1,sz2+2.1,0,0);

    //Create the third line
    displayCubes(sx1,sx2,sy1,sy2,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1,sy2,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1,sy2,sz1+4.2,sz2+4.2,0,0);

    displayCubes(sx1,sx2,sy1+2.1,sy2+2.1,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1+2.1,sy2+2.1,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1+2.1,sy2+2.1,sz1+4.2,sz2+4.2,0,0);

    displayCubes(sx1,sx2,sy1+4.2,sy2+4.2,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+2.1,sx2+2.1,sy1+4.2,sy2+4.2,sz1+4.2,sz2+4.2,0,0);
    displayCubes(sx1+4.2,sx2+4.2,sy1+4.2,sy2+4.2,sz1+4.2,sz2+4.2,0,0);


   glutSwapBuffers();  // Swap the front and back frame buffers (double buffering)
}

/* Called back when timer expired [NEW] */

void timer(int value) {
   glutPostRedisplay();      // Post re-paint request to activate display()
   glutTimerFunc(refreshMills, timer, 0); // next timer call milliseconds later
}

/* Handler for window re-size event. Called back when the window first appears and
   whenever the window is re-sized with its new width and height */

void changeSize(int w, int h)
{
    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if (h == 0)
    h = 1;
    float ratio = w * 1.0 / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

    // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(45.0f, ratio, 0.1f, 100.0f);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);

}

/* Handler for arrow push. Called back when the user press one of the arrow of his keyboard */

void arrow(int arrow, int mousex, int mousey)
{
    float neorayon=0.0f;
    float fraction = 0.1f;
    switch (arrow)
    {
        case GLUT_KEY_LEFT : //Left key
            angle=angle-0.1f;
            camerax=sin(angle)*rayon;
            cameraz= -cos(angle)*rayon;
            break;

        case GLUT_KEY_RIGHT: // Right key
            angle=angle+0.1f;
            camerax=sin(angle)*rayon;
            cameraz=-cos(angle)*rayon;
            neorayon=sqrt(pow((3.1-camerax),2)+pow(3.1-cameraz,2));
            break;

        case GLUT_KEY_DOWN: // Down key
            cameray=cameray-1.0f;
            break;

        case GLUT_KEY_UP: // Up key
            cameray=cameray+1.0f;
            break;
    }
}

/* Rotate the face rotate from axe given in parameter */

void* rotateface(void* ptr){

    int p_direction;
    p_direction=(int)ptr;
    switch(p_direction){
        case (4):
            flag=true;
            for (int i=0;i<60;i++){
                transcube=transcube+(6.2f/60);
                //transcubey=transcubey-(2.0f/60);
                angleCube=angleCube+(90.0f/60);
                display(); //actualise l'affichage des cubes
                Sleep(10);
            }
            flag=false;
            break;
        case (3):
            flag=true;
            for (int i=0;i<60;i++){
                transcube=transcube-(6.2f/60);
                angleCube=angleCube-(90.0f/60);
                display();
                Sleep(10);
            }
            flag=false;
            break;

    }
}

void processNormalKeys(unsigned char key, int x, int y)
{
    pthread_t t;
    int sens;
    int pid;

    // Every enable key have something to execute
    switch (key)
    {
        // Exit the programm when Escape is press
        case (27) :
            exit(0);
            break;

        // Make a left rotation when letter "q" is pressed
        case (113):
            sens=3;
            if (flag==false){
                pid=pthread_create(&t,NULL,rotateface,(void*)sens);
            }
            break;

        // Make right rotation when letter "d" is pressed
        case (100):
            sens=4;
            if (flag==false){
                pid=pthread_create(&t,NULL,rotateface,(void*)sens);
            }

            //pthread_exit(NULL);
            //angleCube=angleCube+1.0f;
            /*angleCube=angleCube+(90.0f/10);
            if (transcube<6.1f){
                transcube=transcube+(6.2f/10);
            }else{
                    if (transcubey<6.1f){
                    transcubey=transcubey+(6.2f/10);
                }else{
                    transcubey=transcubey-(6.2f/10);
                }
            }*/


            //transcube=transcube+0.01f;
            break;

    }
}

/* Main function: GLUT runs as a console application starting at main() */

int main(int argc, char** argv) {
    glutInit(&argc, argv);                                       // Initialize GLUT
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);   // Enable double buffered mode
    glutInitWindowSize(1280, 720);                               // Set the window's initial width & height
    glutInitWindowPosition(50, 50);                              // Position the window's initial top-left corner
    glutCreateWindow(title);                                     // Create window with the given title
    glutDisplayFunc(display);                                    // Register callback handler for window re-paint event
    glutReshapeFunc(changeSize);                                 // Register callback handler for window re-size event
    glutIdleFunc(display);                                       // Register callback handler for element printing into the window
    glutKeyboardFunc(processNormalKeys);                         // Register callback handler for pushing any letter in keyboard
    glutSpecialFunc(arrow);                                      // Register callback handler for pressing arrow keyboard
    initGL();                                                    // Our own OpenGL initialization
    glutTimerFunc(0, timer, 0);                                  // First timer call immediately [NEW]
    glutMainLoop();                                              // Enter the infinite event-processing loop
    return 0;
}

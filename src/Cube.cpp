#include "Cube.h"
#include <cstdio>
#include <windows.h>  // for MS Windows
#include <GL/glut.h>


Cube::Cube()
{
    //ctor
}

void Cube::displayCubes(GLfloat p_sx1,GLfloat p_sx2,GLfloat p_sy1,GLfloat p_sy2,GLfloat p_sz1,GLfloat p_sz2){

    // Render a color-cube consisting of 6 quads with different colors
   //glLoadIdentity();                 // Reset the model-view matrix
   //glTranslatef(0.0f, 0.0f, -7.0f);  // Move right and into the screen
   //glRotatef(0.0f, 1.0f, 1.0f, 1.0f);  // Rotate about (1,1,1)-axis [NEW]

   glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads

      // Top face (y = 1.0f)
      // Define vertices in counter-clockwise (CCW) order with normal pointing out
      glColor3f(0.0f, 1.0f, 0.0f);     // Green
      glVertex3f( p_sx1, p_sy1, p_sz2);
      glVertex3f(p_sx2, p_sy1, p_sz2);
      glVertex3f(p_sx2, p_sy1,  p_sz1);
      glVertex3f( p_sx1, p_sy1,  p_sz1);

      // Bottom face (y = -1.0f)
      glColor3f(1.0f, 0.5f, 0.0f);     // Orange
      glVertex3f( p_sx1, p_sy2,  p_sz1);
      glVertex3f(p_sx2, p_sy2,  p_sz1);
      glVertex3f(p_sx2, p_sy2, p_sz2);
      glVertex3f( p_sx1, p_sy2, p_sz2);

      // Front face  (z = 1.0f)
      glColor3f(1.0f, 0.0f, 0.0f);     // Red
      glVertex3f( p_sx1,  p_sy1, p_sz1);
      glVertex3f(p_sx2,  p_sy1, p_sz1);
      glVertex3f(p_sx2, p_sy2, p_sz1);
      glVertex3f( p_sx1, p_sy2, p_sz1);

      // Back face (z = -1.0f)
      glColor3f(1.0f, 1.0f, 0.0f);     // Yellow
      glVertex3f( p_sx1, p_sy2, p_sz2);
      glVertex3f(p_sx2, p_sy2, p_sz2);
      glVertex3f(p_sx2,  p_sy1, p_sz2);
      glVertex3f( p_sx1,  p_sy1, p_sz2);

      // Left face (x = -1.0f)
      glColor3f(0.0f, 0.0f, 1.0f);     // Blue
      glVertex3f(p_sx2,  p_sy1,  p_sz1);
      glVertex3f(p_sx2,  p_sy1, p_sz2);
      glVertex3f(p_sx2, p_sy2, p_sz2);
      glVertex3f(p_sx2, p_sy2,  p_sz1);

      // Right face (x = 1.0f)
      glColor3f(1.0f, 0.0f, 1.0f);     // Magenta
      glVertex3f(p_sx1,  p_sy1, p_sz2);
      glVertex3f(p_sx1,  p_sy1,  p_sz1);
      glVertex3f(p_sx1, p_sy2,  p_sz1);
      glVertex3f(p_sx1, p_sy2, p_sz2);
   glEnd();  // End of drawing color-cube

}

void bittee(){
    printf("lalalaal");
}

Cube::~Cube()
{
    //dtor
}
